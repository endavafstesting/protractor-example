var homePage = require('../pages/homePage.js');
var apiPage = require('../pages/protractorApiPage.js');

describe('Protractor API page', function () {
  
  beforeEach(function () {
    browser.get(homePage.url);
    homePage.navigateToProtractorApiPage();
  });
  
  it('I should see Protractor Api page when navigating with the menu', function () {
    expect(browser.getCurrentUrl()).toEqual('https://www.protractortest.org/#/api');
  });
  
  it('Left list should be updated on search', function () {
    apiPage.searchFor('waitFor');
    expect(apiPage.searchElementsList.count()).toBe(2);
    expect(apiPage.searchElementsList.get(0).getText()).toEqual('waitForAngularEnabled');
    expect(apiPage.searchElementsList.last().getText()).toEqual('waitForAngular');
  });
  
  it('Title should be updated when clicking left items', function () {
    element(by.id('searchInput')).sendKeys('waitFor');
    var list = element.all(by.binding('item.displayName'));
    list.get(0).click();
    var text;
    
    list.get(0).getText().then(function (text1) {
      text = text1;
      console.log(text);
      expect(element(by.css('.api-title.ng-binding')).getText()).toContain(text);
    });
    
  });
  
});
