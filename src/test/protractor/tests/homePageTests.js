var homePage = require('../pages/homePage.js');

describe('Home Page tests', function () {
  beforeEach(function () {
    browser.get(homePage.url);
  });
  
  it('I should see Protractor logo on Home page', function () {
    expect(homePage.protractorLogo.isDisplayed()).toBe(true);
  });
  
});