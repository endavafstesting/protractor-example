var homePage = function () {
  this.url = 'https://www.protractortest.org/#/';
  
  this.protractorLogo = element(by.css('.protractor-logo'));
  this.referenceMenu = element(by.id('drop4'));
  this.protractorApiMenuItem = element(by.css('[href="#/api"]'));
  
  this.navigateToProtractorApiPage = function () {
    this.referenceMenu.click();
    this.protractorApiMenuItem.click();
  };
  
};

module.exports = new homePage();