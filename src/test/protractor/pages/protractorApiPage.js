var protractorApiPage = function () {
  
  this.searchInput = element(by.id('searchInput'));
  this.searchElementsList = element.all(by.binding('item.displayName'));
  
  this.searchFor = function (text) {
    this.searchInput.sendKeys(text);
  };
};

module.exports = new protractorApiPage();