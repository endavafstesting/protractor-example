var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  capabilities:
    {'browserName': 'chrome'},
  
  baseUrl: 'https://www.protractortest.org/#/',
  
  onPrepare: function(){
    browser.driver.manage().window().maximize();
    jasmine.getEnv().addReporter(
      new Jasmine2HtmlReporter({
        savePath: 'target/screenshots'
      })
    );
  },
  
  //specs: ['spec.js'],
  suites:{
    homePage: ['./tests/homePageTests.js'],
    apiPage: ['./tests/protractorApiTest.js']
  },
  
  jasmineNodeOpts: {
    isVerbose: false,
    showColors: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 20000
  }
};
