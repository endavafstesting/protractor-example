var {defineSupportCode} = require('cucumber');

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
var expect = chai.expect;

defineSupportCode(({Before, After}) => {

  
  Before({tags: '@slow and @large and not @skip'}, ()=> {
    return browser.sleep(5000);
  })
});