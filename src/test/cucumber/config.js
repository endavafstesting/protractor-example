exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect: true,
  capabilities: {
  browserName: 'chrome',
    shardTestFiles: false,
    maxInstances: 10,
    chromeOptions: {
    args: [ "--headless", "--disable-gpu", "--window-size=1920,1024" ]
  }
},


baseUrl: 'https://www.protractortest.org/#/',
  
  // Cucumber setup
  framework: 'custom',
  // path relative to the current config file
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  // Spec patterns are relative to this directory.
  specs: [
    './features/*.feature'
  ],
  cucumberOpts: {
    strict: true,
    require: ['hooks.js', './steps/*.steps.js'],
    format: [],
    profile: false,
    'no-source': true,
    tags: ['@smoke', '~@skip']
  }
};
