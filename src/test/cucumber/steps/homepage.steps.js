const {defineSupportCode} = require('cucumber');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
const expect = chai.expect;

defineSupportCode(({Given, When, Then}) => {
 
  const homePage = require('../../protractor/pages/homePage.js');

  Given(/^I have a browser$/, () => {});
  
  When(/^I navigate to protractor page$/, () => {
    return browser.get(homePage.url);
  });
  
  Then(/^I should see Protractor logo on Home page$/, () => {
    return expect(homePage.protractorLogo.isDisplayed()).to.eventually.equal(true);
  });
});