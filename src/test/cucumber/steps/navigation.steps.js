const {defineSupportCode} = require('cucumber');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
const expect = chai.expect;

defineSupportCode(({Given, When, Then}) => {
  
  When(/^I navigate to ([^\"]*)$/, (pageUrl) => {
    return browser.get(pageUrl);
  });
  
  Then(/^The page title should be ([^\"]*)$/, (pageTitle) => {
    return expect(element(by.css("h1")).getText()).to.eventually.equal(pageTitle);
  });
});