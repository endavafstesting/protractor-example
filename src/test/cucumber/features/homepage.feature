@smoke
Feature: Protractor Home page
  As a user
  I should be able to see the protractor home page
  In order to read more about protractor

  Scenario Outline: When i load the protractor home page I should see the logo
    Given I have a browser
    When I navigate to <pageUrl>
    Then The page title should be <pageTitle>

    Examples:
      | pageUrl                                           | pageTitle       |
      | https://www.protractortest.org/#/getting-started  | Getting Started |
      | https://www.protractortest.org/#/tutorial         | Tutorial        |

